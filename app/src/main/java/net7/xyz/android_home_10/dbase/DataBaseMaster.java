package net7.xyz.android_home_10.dbase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


import java.util.ArrayList;
import java.util.List;

import net7.xyz.android_home_10.model.Country;
import net7.xyz.android_home_10.model.User;

import static net7.xyz.android_home_10.dbase.DataBaseCreator.Countries.COUNTRY_CAPITAL;
import static net7.xyz.android_home_10.dbase.DataBaseCreator.Countries.COUNTRY_NAME;
import static net7.xyz.android_home_10.dbase.DataBaseCreator.Countries.COUNTRY_POPULATION;
import static net7.xyz.android_home_10.dbase.DataBaseCreator.Countries.COUNTRY_REGION;
import static net7.xyz.android_home_10.dbase.DataBaseCreator.Countries.COUNTRY_SUBREGION;
import static net7.xyz.android_home_10.dbase.DataBaseCreator.Users.USER_COUNTRY;
import static net7.xyz.android_home_10.dbase.DataBaseCreator.Users.USER_NAME;
import static net7.xyz.android_home_10.dbase.DataBaseCreator.Users.USER_PASS;


public class DataBaseMaster {

    private SQLiteDatabase database;
    private DataBaseCreator dbCreator;
    private static DataBaseMaster instance;

    private DataBaseMaster(Context context) {

        dbCreator = new DataBaseCreator(context);
        if (database == null || database.isOpen()) {
            database = dbCreator.getWritableDatabase();
        }
    }

    public static DataBaseMaster getInstance(Context context) {
        if (instance == null) {
            instance = new DataBaseMaster(context);
        }
        return instance;
    }

    public long insertUser(User user) {
        ContentValues cv = new ContentValues();
        cv.put(USER_NAME, user.getName());
        cv.put(USER_PASS, user.getPass());
        cv.put(USER_COUNTRY, user.getCountry());
        return database.insert(DataBaseCreator.Users.TABLE_NAME, null, cv);
    }

    public long insertCountry(Country country) {
        ContentValues cv = new ContentValues();
        cv.put(COUNTRY_NAME, country.getName());
        cv.put(COUNTRY_REGION, country.getRegion());
        cv.put(COUNTRY_SUBREGION, country.getSubregion());
        cv.put(COUNTRY_CAPITAL, country.getCapital());
        cv.put(COUNTRY_POPULATION, country.getPopulation());

        return database.insert(DataBaseCreator.Countries.TABLE_NAME, null, cv);
    }

    public List<String> getUsers() {

        String query = " SELECT " + USER_NAME + " FROM " + DataBaseCreator.Users.TABLE_NAME;
        Cursor cursor = database.rawQuery(query, null);
        List<String> list = new ArrayList<>();
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(cursor.getColumnIndex(USER_NAME)));
            cursor.moveToNext();
            Log.d("database", "getUsers: :");
        }
        cursor.close();
        return list;
    }

    public String getUserCountry(User user) {

        String query = " SELECT " + USER_COUNTRY + " FROM " + DataBaseCreator.Users.TABLE_NAME +
                " WHERE " + USER_NAME + " =  '" + user.getName() + "'";
        Cursor cursor = database.rawQuery(query, null);
        List<String> list = new ArrayList<>();
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(cursor.getColumnIndex(USER_COUNTRY)));
            cursor.moveToNext();
            Log.d("database", "getUsers: :");
        }
        cursor.close();
        return list.get(0);
    }

    public List<User> getUsersAndPass() {

        String query = " SELECT " + USER_NAME + "," + USER_PASS + " FROM " + DataBaseCreator.Users.TABLE_NAME;
        Cursor cursor = database.rawQuery(query, null);
        List<User> list = new ArrayList<User>();
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            User user = new User();
            user.setName(cursor.getString(cursor.getColumnIndex(USER_NAME)));
            user.setPass(cursor.getString(cursor.getColumnIndex(USER_PASS)));
            list.add(user);
            cursor.moveToNext();
            Log.d("database", "getUsersAndPass: :");
        }
        cursor.close();
        return list;
    }


    public List<String> getCountries() {

        String query = " SELECT " + COUNTRY_NAME + " FROM " + DataBaseCreator.Countries.TABLE_NAME;
        Cursor cursor = database.rawQuery(query, null);

        if (cursor != null) {
            List<String> list = new ArrayList<>();
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                list.add(cursor.getString(cursor.getColumnIndex(COUNTRY_NAME)));
                cursor.moveToNext();
                Log.d("database", "getCountries: :");
            }
            cursor.close();
            return list;
        } else {
            return null;
        }

    }

    public String getSubregion(User user) {

        String query = " SELECT " + COUNTRY_SUBREGION + " FROM " + DataBaseCreator.Countries.TABLE_NAME +
                " WHERE " + COUNTRY_NAME + " =  '" + user.getCountry() + "'";

        Cursor cursor = database.rawQuery(query, null);

        if (cursor != null) {
            List<String> list = new ArrayList<>();
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                list.add(cursor.getString(cursor.getColumnIndex(COUNTRY_SUBREGION)));
                cursor.moveToNext();
                Log.d("database", "getSubregion: :");
            }
            cursor.close();
            return list.get(0);
        } else {
            return null;
        }

    }

    public List<Country> getCountriesListFromRegion(String subregion) {
        if (database == null || database.isOpen()) {
            database = dbCreator.getReadableDatabase();
        }

        String query = " SELECT " + " * " + " FROM " + DataBaseCreator.Countries.TABLE_NAME +
                " WHERE " + COUNTRY_SUBREGION + " = '" + subregion + "'";

        Cursor cursor = database.rawQuery(query, null);

        if (cursor != null) {
            List<Country> list = new ArrayList<>();
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Country country = new Country();
                country.setName(cursor.getString(cursor.getColumnIndex(COUNTRY_NAME)));
                country.setRegion(cursor.getString(cursor.getColumnIndex(COUNTRY_REGION)));
                country.setSubregion(cursor.getString(cursor.getColumnIndex(COUNTRY_SUBREGION)));
                country.setCapital(cursor.getString(cursor.getColumnIndex(COUNTRY_CAPITAL)));
                country.setPopulation(cursor.getInt(cursor.getColumnIndex(COUNTRY_POPULATION)));
                list.add(country);
                cursor.moveToNext();
                Log.d("database", "getCountriesFromRegion: :");
            }
            cursor.close();
            return list;
        } else {
            return null;
        }
    }
}


