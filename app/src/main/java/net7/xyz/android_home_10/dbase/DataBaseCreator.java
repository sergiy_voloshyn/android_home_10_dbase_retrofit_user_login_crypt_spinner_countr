package net7.xyz.android_home_10.dbase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class DataBaseCreator extends SQLiteOpenHelper {
    public static final String DB_NAME = "db_name";
    public static final int DB_VERSION = 1;

    public static class Users implements BaseColumns {
        public static final String TABLE_NAME = "t_users";
        public static final String USER_NAME = "user_name";
        public static final String USER_PASS = "user_pass";
        public static final String USER_COUNTRY = "user_country";
    }

    static String SCRIPT_CREATE_TBL_USERS = "CREATE TABLE " + Users.TABLE_NAME + " (" +
            Users._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + Users.USER_NAME + " TEXT, " +
            Users.USER_PASS + " TEXT, " + Users.USER_COUNTRY + " TEXT " +
            ");";

    public static class Countries implements BaseColumns {
        public static final String TABLE_NAME = "t_countries";
        public static final String COUNTRY_NAME = "country_name";
        public static final String COUNTRY_REGION = "country_region";
        public static final String COUNTRY_SUBREGION = "country_subregion";
        public static final String COUNTRY_CAPITAL = "country_capital";
        public static final String COUNTRY_POPULATION = "country_population";
    }

    static String SCRIPT_CREATE_TBL_COUNTRIES = "CREATE TABLE " + Countries.TABLE_NAME + " (" +
            Countries._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + Countries.COUNTRY_NAME + " TEXT, " +
            Countries.COUNTRY_REGION + " TEXT, " + Countries.COUNTRY_SUBREGION + " TEXT, " +
            Countries.COUNTRY_CAPITAL + " TEXT, " + Countries.COUNTRY_POPULATION + " INTEGER " +
            ");";


    public DataBaseCreator(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SCRIPT_CREATE_TBL_USERS);
        sqLiteDatabase.execSQL(SCRIPT_CREATE_TBL_COUNTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
