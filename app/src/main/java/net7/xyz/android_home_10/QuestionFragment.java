package net7.xyz.android_home_10;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import net7.xyz.android_home_10.model.Country;
import net7.xyz.android_home_10.model.Statistics;

public class QuestionFragment extends Fragment {


    @BindView(R.id.country_question)
    EditText countryQuestion;

    @BindView(R.id.capital_answer)
    EditText capitalAnswer;

    List<Country> countryList;
    Statistics statistics;
    int counter = 0;

    private OnFragmentInteractionListener mListener;

    public static QuestionFragment newInstance() {
        QuestionFragment fragment = new QuestionFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_question, container, false);
        ButterKnife.bind(this, v);

        if (mListener != null) {

            countryList = mListener.getCountriesListFromSubregion();
        }

        Collections.shuffle(countryList);
        countryQuestion.setText(countryList.get(counter).getName());
        statistics = new Statistics();
        return v;
    }

    @OnClick(R.id.button_question)
    public void onQuestionButtonPressed() {

        for (int i = 0; i < countryList.size(); i++) {
            String country = countryList.get(i).getName();
            String findCountry = countryQuestion.getText().toString();

            if (country.equals(findCountry)) {
                if (capitalAnswer.getText().toString().toLowerCase().equals(countryList.get(i).getCapital().toLowerCase())) {
                    statistics.incRight();
                    counter++;
                    countryQuestion.setText(countryList.get(counter).getName());
                    break;
                } else {
                    statistics.incWrong();
                    counter++;
                    countryQuestion.setText(countryList.get(counter).getName());
                    break;
                }
            }
        }
        if (statistics.getCount() == 5 || counter == countryList.size() - 1) {

            new AlertDialog.Builder(getContext())
                    .setTitle("Your knowledge is great!")
                    .setMessage(statistics.toString())
                    .setCancelable(true)
                    .show();
            statistics.setCount(0);
            if (counter == countryList.size() - 1) counter = 0;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        List<Country> getCountriesListFromSubregion();
    }
}
