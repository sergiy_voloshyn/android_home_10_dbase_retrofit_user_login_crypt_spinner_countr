package net7.xyz.android_home_10.network;

import java.util.List;

import net7.xyz.android_home_10.model.Country;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;


public class Retrofit {

    private static final String ENDPOINT = "http://restcountries.eu/rest";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    interface ApiInterface {
        //https://restcountries.eu/rest/v2/all?
        @GET("/v2/all")
        void getAllInformation(Callback<List<Country>> callback);
    }

    private static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getAllInformation(Callback<List<Country>> callback) {

        apiInterface.getAllInformation(callback);
    }
}
