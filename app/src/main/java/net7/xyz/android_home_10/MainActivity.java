package net7.xyz.android_home_10;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.DigestException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import net7.xyz.android_home_10.dbase.DataBaseMaster;
import net7.xyz.android_home_10.model.Country;
import net7.xyz.android_home_10.model.User;
import net7.xyz.android_home_10.network.Retrofit;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity implements RegistrationFragment.OnFragmentInteractionListener,
        LoginFragment.OnFragmentInteractionListener, QuestionFragment.OnFragmentInteractionListener {

    RegistrationFragment registrationFragment;
    LoginFragment loginFragment;
    QuestionFragment questionFragment;

    DataBaseMaster dataBaseMaster;
    User mainUser;
    List<String> countryList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        registrationFragment = RegistrationFragment.newInstance();
        loginFragment = LoginFragment.newInstance();
        questionFragment = QuestionFragment.newInstance();

        mainUser = new User();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.base_container, registrationFragment)
                .addToBackStack(null)
                .commit();

        dataBaseMaster = DataBaseMaster.getInstance(this);
        //get countries list from database
        countryList = dataBaseMaster.getCountries();
        if (countryList.size() != 0) {
            countryList = dataBaseMaster.getCountries();
            List<String> listUsers = dataBaseMaster.getUsers();
            if (listUsers.size() != 0) {

                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.base_container, loginFragment)
                        .addToBackStack(null)
                        .commit();
            }

        } else {
            //get data from server
            Retrofit.getAllInformation(new Callback<List<Country>>() {
                @Override
                public void success(List<Country> countries, Response response) {
                    try {
                        for (int i = 0; i < countries.size(); i++) {
                            dataBaseMaster.insertCountry(countries.get(i));
                        }
                        countryList = dataBaseMaster.getCountries();
                        registrationFragment.onListUpdate(countryList);

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        new AlertDialog.Builder(MainActivity.this)
                                .setTitle("Alert")
                                .setMessage(ex.toString())
                                .setCancelable(true)
                                .show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Alert")
                            .setMessage(error.toString())
                            .setCancelable(true)
                            .show();
                }
            });
        }
    }

    @Override
    public void onLoginInteraction(String name, String pass) {

        if (checkUserLogin(name, pass)) {
            mainUser.setCountry(dataBaseMaster.getUserCountry(mainUser));
            mainUser.setSubregion(dataBaseMaster.getSubregion(mainUser));

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.base_container, questionFragment)
                    .addToBackStack(null)
                    .commit();
        }
    }

    public boolean checkUserLogin(String name, String pass) {
        List<User> listUsers = dataBaseMaster.getUsersAndPass();
        if (listUsers.size() != 0) {
            Boolean okPass = false;
            Boolean okName = false;
            String passEncrypted = getEncryptedMessage(pass);

            for (int i = 0; i < listUsers.size(); i++) {
                if (listUsers.get(i).getName().equals(name.toLowerCase())) {
                    okName = true;
                    mainUser.setName(listUsers.get(i).getName().toLowerCase());
                    if (listUsers.get(i).getPass().equals(passEncrypted)) {
                        okPass = true;
                        mainUser.setPass(listUsers.get(i).getPass());
                    }
                }
            }
            if (!okName && !okPass) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Name and password incorrect!")
                        .setMessage("Please input correct name and password!")
                        .setCancelable(true)
                        .show();
                return false;
            } else if (!okPass) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Password incorrect!")
                        .setMessage("Please input correct password")
                        .setCancelable(true)
                        .show();
                return false;
            } else if (!okName) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Name incorrect!")
                        .setMessage("Please input correct name!")
                        .setCancelable(true)
                        .show();
                return false;
            } else if (okName && okPass) {
                //all is ok
                return true;
            }
        }
        return false;
    }

    @Override
    public List<Country> getCountriesListFromSubregion() {

        mainUser.setCountry(dataBaseMaster.getUserCountry(mainUser));
        mainUser.setSubregion(dataBaseMaster.getSubregion(mainUser));

        return dataBaseMaster.getCountriesListFromRegion(mainUser.getSubregion());
    }

    @Override
    public void onRegistrationInteraction(User user) {

        String passEncrypted = getEncryptedMessage(user.getPass());
        mainUser = user;
        mainUser.setPass(passEncrypted);
        dataBaseMaster.insertUser(user);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.base_container, questionFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public List<String> getCountryName() {
        if (countryList != null) return countryList;
        else return null;
    }


    String getEncryptedMessage(String text) {
// {"MD5", "SHA-1", "SHA-256"};
        String algorithm = "SHA-1";
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance(algorithm);
            md.update(text.getBytes("UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (md != null) {
            return new BigInteger(1, md.digest()).toString(16);
        } else {
            return null;
        }

    }

}

