package net7.xyz.android_home_10;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import net7.xyz.android_home_10.model.User;


public class RegistrationFragment extends Fragment {

    @BindView(R.id.name_registration)
    EditText name;
    @BindView(R.id.pass_registration)
    EditText pass;
    @BindView(R.id.spinner_country_registration)

    Spinner countrySpinner;
    ArrayAdapter<String> adapter;
    List<String> list;
    public OnFragmentInteractionListener mListener;


    public static RegistrationFragment newInstance() {
        RegistrationFragment fragment = new RegistrationFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_registration, container, false);
        ButterKnife.bind(this, v);

        //получаем список
        list = mListener.getCountryName();
        //если данных нет - создаем пустой список, который будет впоследствии обновляться
        if (list == null) {
            list = new ArrayList<>();
        }
        adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countrySpinner.setAdapter(adapter);
        return v;
    }


    @OnClick(R.id.button_registration)
    public void buttonRegistration() {
        if (mListener != null) {
            if (checkUser()) {
                User user = new User(name.getText().toString(), pass.getText().toString(),
                        countrySpinner.getSelectedItem().toString());
                mListener.onRegistrationInteraction(user);
            }
        }
    }

    public Boolean checkUser() {
        String tmpName = name.getText().toString().trim();
        if (tmpName.equals("")) {
            new AlertDialog.Builder(getContext())
                    .setTitle("User name")
                    .setMessage("User name is empty!")
                    .setCancelable(true)
                    .show();
            return false;
        }
        String tmpPass = pass.getText().toString().trim();
        if (tmpPass.equals("") || tmpPass.length() < 6) {
            new AlertDialog.Builder(getContext())
                    .setTitle("User password ")
                    .setMessage("User password is simple!\n" +
                            "Length must be 6 symbols and no space!")
                    .setCancelable(true)
                    .show();
            return false;
        }
        String tmpCountry = countrySpinner.getSelectedItem().toString().trim();
        if (tmpCountry.equals("") || tmpCountry.length() < 2) {
            new AlertDialog.Builder(getContext())
                    .setTitle("Country name ")
                    .setMessage("User country is wrong!\n" +
                            "Input correct country name!")
                    .setCancelable(true)
                    .show();
            return false;
        }
        //all is ok
        return true;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onRegistrationInteraction(User user);

        List<String> getCountryName();
    }

    public void onListUpdate(List<String> list) {
        if (list != null && !list.isEmpty()) {
            //обновляем список. Именно обновляем тот объект, который передали в адаптер,
            //а не просто перезаписываем ссылку this.list = list;
            this.list.clear();
            this.list.addAll(list);
            this.adapter.notifyDataSetChanged();
        }

    }

}

