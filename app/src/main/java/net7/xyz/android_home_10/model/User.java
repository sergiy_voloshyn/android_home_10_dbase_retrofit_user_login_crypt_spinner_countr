package net7.xyz.android_home_10.model;

public class User {
    Long id;
    String name;
    String pass;
    String country;
    String subregion;


    public User() {
        this.name = "";
        this.pass = "";
        this.country = "";
        this.subregion = "";

    }

    public User(String name, String pass, String country) {
        this.name = name;
        this.pass = pass;
        this.country = country;
        this.subregion = "";
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public String getPass() {
        return pass;
    }

    public String getCountry() {
        return country;
    }

    public String getSubregion() {
        return subregion;
    }

    public void setSubregion(String subregion) {
        this.subregion = subregion;
    }


}
