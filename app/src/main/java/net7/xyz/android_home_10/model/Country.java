package net7.xyz.android_home_10.model;

import java.io.Serializable;

public class Country implements Serializable {

    public String name;
    public String region;
    public String subregion;
    public String capital;
    public int population;

    public Country() {
        this.name = "";
        this.region = "";
        this.subregion = "";
        this.capital = "";
        this.population = 0;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setSubregion(String subregion) {
        this.subregion = subregion;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public String getName() {
        return name;
    }

    public String getRegion() {
        return region;
    }

    public String getSubregion() {
        return subregion;
    }

    public String getCapital() {
        return capital;
    }

    public int getPopulation() {
        return population;
    }
}
