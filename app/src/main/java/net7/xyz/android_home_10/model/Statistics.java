package net7.xyz.android_home_10.model;

public class Statistics {
    int right;
    int wrong;
    int count;

    public Statistics() {
        this.right = 0;
        this.wrong = 0;
        this.count = 0;
    }

    @Override
    public String toString() {
        return "Statistics:\n" +
                "right=" + right + "\n" +
                "wrong=" + wrong + "\n" +
                "count=" + count + "\n";
    }

    public void setCount(int count) {
        this.count = count;
        this.right = count;
        this.wrong = count;
    }

    public int getCount() {
        return count;
    }

    public void incRight() {
        this.right = this.right + 1;
        this.count = this.count + 1;
    }

    public void incWrong() {
        this.wrong = this.wrong + 1;
        this.count = this.count + 1;
    }
}
